<?php

return [
    'CLOUD_NAME' => env('CLOUD_NAME'),
    'API_KEY' => env('CLOUDINARY_API_KEY'),
    'API_SECRET' => env('CLOUDINARY_API_SECRET'),
];
