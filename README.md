<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## How to run the app using docker
1. Run `docker-compose up -d`
2. Run `docker-compose exec db bash`
3. Run `mysql -u root -p` with gonnet paswword
4. Run `GRANT ALL ON gonnet.* TO 'gonnet'@'%' IDENTIFIED BY 'gonnet';`
5. Run `FLUSH PRIVILEGES;`
6. Run `docker-compose exec app bash`
7. Run `php artisan migrate`
8. You can access the app through http://localhost/ directly
