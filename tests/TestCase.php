<?php

namespace Tests;

use App\Models\User;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    protected function seedUserAndPostTypes()
    {
        $this->artisan('db:seed --class=UserTypeSeeder');
        $this->artisan('db:seed --class=PostTypeSeeder');
    }

    protected function installPassportAndSetupUser()
    {
        \Artisan::call('passport:install', ['-vvv' => true]);
        $this->user = User::factory()->create(['email' => 'usef@usef.com', 'user_type_id' => 2]);
        $token = $this->user->createToken('Laravel Password Grant Client')->accessToken;
        $this->user['token'] = $token;
        $this->withHeaders(['Authorization' => "Bearer " . $token]);
    }

    protected function createUser($fields = [])
    {
        return User::factory()->create(['user_type_id' => 2, ...$fields]);
    }

    protected function createAdminUser()
    {
        return User::factory()->create(['user_type_id' => 1]);
    }
}
