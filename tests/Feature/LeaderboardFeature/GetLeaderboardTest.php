<?php

namespace Tests\Feature\LeaderboardFeature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class GetLeaderboardTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        $this->seedUserAndPostTypes();
        $this->installPassportAndSetupUser();
    }

    public function testGetLeaderboardReturns200()
    {
        $response = $this->get('/api/leaderboard');
        $response->assertStatus(200);
    }

    public function testLeaderboardReturnsUsersInDescOrder()
    {
        $user1 = $this->createUser(['points' => 150]);
        $user2 = $this->createUser(['points' => 200]);
        $user3 = $this->createUser(['points' => 50]);
        $response = $this->get('/api/leaderboard');

        $expectedResult = [
            ['id' => $user2->id, 'name' => $user2->name, 'points' => $user2->points],
            ['id' => $user1->id, 'name' => $user1->name, 'points' => $user1->points],
            ['id' => $user3->id, 'name' => $user3->name, 'points' => $user3->points],
        ];

        $response->assertSeeInOrder(...$expectedResult);
    }
}
