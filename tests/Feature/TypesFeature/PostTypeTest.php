<?php

namespace Tests\Feature\TypesFeature;

use App\Models\PostType;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class PostTypeTest extends TestCase
{
    use RefreshDatabase, WithoutMiddleware;

    public function testShouldReturnAllPostTypes()
    {
        $response = $this->get('/api/post-types');

        $response->assertOk();
        $response->assertJsonCount(PostType::count());
    }

    public function testShouldPresistNewPostTypeInDb()
    {
        $response = $this->post('/api/post-types', ["name" => 'testType' ]);

        $response->assertStatus(201);
        $this->assertDatabaseHas('post_types', ['name' => 'testType']);
    }
}
