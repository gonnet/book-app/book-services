<?php

namespace Tests\Feature\TypesFeature;

use App\Models\PostType;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\Helpers\TestHelper;
use Tests\TestCase;

class UserTypesTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
        $this->seedUserAndPostTypes();
        $this->installPassportAndSetupUser();
    }

    public function testNewUserShouldHaveDefaultTypeUser()
    {
        $response = $this->post('/api/register', [
            'name' => 'Usef',
            'email' => 'abc@xyz.com',
            'password' => '123123123'
        ]);

        $response->assertStatus(201)->assertJson(['user_type_id' => 2]);
    }

    public function testNormalUserCannotChangePostStatus()
    {
        $postType = PostType::first();
        $post = $this->post('/api/posts', [
            'title' => 'test title',
            'description' => 'desc test',
            'image_url' => 'img1.jpg',
            'pickup_location' => 'Egypt',
            'contact_method' => 'Phone',
            'post_type_id' => $postType->id
        ]);

        $response = $this->patch('/api/posts/' . $post['id'] . '/status', ['status' => 'ACTIVE']);

        $response->assertForbidden();
    }
    
    public function testAdminCanChangePostStatus()
    {
        $postType = PostType::first();
        $post = $this->post('/api/posts', [
            'title' => 'test title',
            'description' => 'desc test',
            'image_url' => 'img1.jpg',
            'pickup_location' => 'Egypt',
            'contact_method' => 'Phone',
            'post_type_id' => $postType->id
        ]);

        $user = User::factory()->create(['user_type_id' => 1]);

        $response = $this->actingAs($user)->patch('/api/posts/' . $post['id'] . '/status', ['status' => 'ACTIVE']);

        $response->assertOk();
    }
}
