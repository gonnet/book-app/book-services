<?php

namespace Tests\Feature;

use App\Models\Notification as ModelsNotification;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class NotificationTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
        $this->seedUserAndPostTypes();
        $this->installPassportAndSetupUser();
    }

    public function testSettingFcmTokenReturns201()
    {
        $response = $this->post('/api/users/setFcmToken', ['token' => '1234']);
        $response->assertStatus(201)->assertJsonStructure(['token', 'user_id']);
    }

    public function testSettingFcmTokenTwiceReturnsOldToken()
    {
        $this->post('/api/users/setFcmToken', ['token' => '1234']);
        $response = $this->post('/api/users/setFcmToken', ['token' => '1234']);
        $response->assertStatus(200)->assertJsonStructure(['token', 'user_id']);
    }

    public function testSettingFcmTokenWithoutAuthFails()
    {
        $this->withHeader('Authorization', 'Bearer 1234');
        $response = $this->post('api/users/setFcmToken', ['token' => '1234']);
        $response->assertStatus(401);
    }

    public function testShouldReturnOkWhenSendingTitleAndBodyAndUserIds()
    {
        $this->post('/api/users/setFcmToken', ['token' => '1234']);
        $response = $this->post('api/notifications/send', [
            'title' => 'notification test',
            'body' => 'body test',
            'users' => [$this->user['id']]
        ]);

        $response->assertStatus(200);
    }

    public function testShouldReturnBadRequestWhenSendingInvalidUserIds()
    {
        $response = $this->post('api/notifications/send', [
            'title' => 'notification test',
            'body' => 'body test',
            'users' => [1]
        ]);

        $response->assertStatus(400);
    }
   
    public function testShouldSaveNotificationAfterSent()
    {
        $this->post('/api/users/setFcmToken', ['token' => '1234']);

        $notification = $this->post('api/notifications/send', [
            'title' => 'notification test',
            'body' => 'body test',
            'users' => [$this->user['id']]
        ]);

        $response = $this->get('api/users/notifications');

        $response->assertStatus(200)->assertSee(['title' => 'notification test']);
    }

    public function testNotificationsShouldBeSortedDesc()
    {
        $this->travel(-5)->hours();
        ModelsNotification::create([
            'title' => 'notification test 1',
            'body' => 'body test',
            'user_id' => $this->user['id'],
        ]);

        $this->travelBack();

        ModelsNotification::create([
            'title' => 'notification test 2',
            'body' => 'body test',
            'user_id' => $this->user['id'],
        ]);

        $response = $this->get('api/users/notifications');

        $response->assertStatus(200)->assertSeeInOrder(['notification test 2', 'notification test 1']);
    }

    public function testShouldSendNotificationWhenPostStatusIsUpdated()
    {
        $this->post('/api/users/setFcmToken', ['token' => '1234']);

        $this->postType = $this->post('/api/post-types', ['name' => 'SELL']);

        $post = $this->post('/api/posts', [
            'title' => 'test title',
            'description' => 'desc test',
            'image_url' => 'img1.jpg',
            'pickup_location' => 'Egypt',
            'contact_method' => 'Phone',
            'post_type_id' => $this->postType['id']
        ]);

        $adminUser = User::factory()->create(['user_type_id' => 1]);
        
        $this->actingAs($adminUser)->patch('/api/posts/' . $post['id'] . '/status', ['status' => 'ACTIVE']);

        $this->assertDatabaseHas('notifications', [
            'user_id' => $this->user['id'],
            'title' => 'Your post\'s status has changed',
            'body' => 'Your post status was changed to ACTIVE',
        ]);
    }

    public function testNotificationShouldBeStoredEvenIfUserIsNotLoggedIn()
    {
        $this->postType = $this->post('/api/post-types', ['name' => 'SELL']);

        $post = $this->post('/api/posts', [
            'title' => 'test title',
            'description' => 'desc test',
            'image_url' => 'img1.jpg',
            'pickup_location' => 'Egypt',
            'contact_method' => 'Phone',
            'post_type_id' => $this->postType['id']
        ]);

        $adminUser = User::factory()->create(['user_type_id' => 1]);

        $this->actingAs($adminUser)->patch('/api/posts/' . $post['id'] . '/status', ['status' => 'ACTIVE']);

        $this->assertDatabaseHas('notifications', [
            'user_id' => $this->user['id'],
            'title' => 'Your post\'s status has changed',
            'body' => 'Your post status was changed to ACTIVE',
        ]);
    }
}
