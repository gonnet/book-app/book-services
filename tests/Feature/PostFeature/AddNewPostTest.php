<?php

namespace Tests\Feature\PostFeature;

use App\Models\PostType;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class AddNewPostTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();

        $this->seedUserAndPostTypes();
        $this->installPassportAndSetupUser();

        $this->postType = PostType::where(['name' => 'SELL'])->first();
    }

    public function testAddingPostReturns201()
    {
        $response = $this->post('/api/posts', [
            'title' => 'test title',
            'description' => 'desc test',
            'image_url' => 'img1.jpg',
            'pickup_location' => 'Egypt',
            'contact_method' => 'Phone',
            'post_type_id' => $this->postType['id']
        ]);

        $response->assertStatus(201);
    }

    public function testAddingNewPostSavesToDatabase()
    {
        $this->post('/api/posts', [
            'title' => 'test title',
            'description' => 'desc test',
            'image_url' => 'img1.jpg',
            'pickup_location' => 'Egypt',
            'contact_method' => 'Phone',
            'post_type_id' => $this->postType['id']
        ]);

        $this->assertDatabaseHas('posts', [
            'title' => 'test title'
        ]);
    }

    public function testAddingNewPostHasDefaultStatusOfInReview()
    {
        $this->post('/api/posts', [
            'title' => 'test title',
            'description' => 'desc test',
            'image_url' => 'img1.jpg',
            'pickup_location' => 'Egypt',
            'contact_method' => 'Phone',
            'post_type_id' => $this->postType['id']
        ]);

        $this->assertDatabaseHas('posts', [
            'title' => 'test title',
            'status' => 'INREVIEW'
        ]);
    }

    public function testShouldReturnNotAuthorizedIfUserDoesNotHaveToken()
    {
        $this->withHeaders(['Authorization' => '']);
        $response = $this->post('/api/posts', [
            'title' => 'test title',
            'description' => 'desc test',
            'image_url' => 'img1.jpg',
            'pickup_location' => 'Egypt',
            'contact_method' => 'Phone',
            'post_type_id' => $this->postType['id']
        ]);
        $response->assertUnauthorized();
    }

    public function testShouldReturnRequiredValidationMessagesWhenPassingEmptyPost()
    {
        $response = $this->post('/api/posts', []);
        $response->assertJsonPath('errors', [
            'title' => ['Title is required'],
            'description' => ['Description is required'],
            'image_url' => ['Image is required'],
            'pickup_location' => ['Location is required'],
            'contact_method' => ['Contact method is required'],
            'post_type_id' => ['post type id is required'],
        ]);
    }

    public function testShouldReturnUserDataWithPost()
    {
        $post = $this->post('/api/posts', [
            'title' => 'test title',
            'description' => 'desc test',
            'image_url' => 'img1.jpg',
            'pickup_location' => 'Egypt',
            'contact_method' => 'Phone',
            'post_type_id' => $this->postType['id']
        ]);

        $response = $this->get('/api/posts/' . $post['id']);

        $response->assertOk();
        $response->assertJson([
            'title' => 'test title',
            'user' => ['name' => $this->user['name'], 'phone' => null]
        ]);
    }

    public function testAddingNewPostWithPriceSavesToDatabase()
    {
        $this->post('/api/posts', [
            'title' => 'test title',
            'description' => 'desc test',
            'image_url' => 'img1.jpg',
            'pickup_location' => 'Egypt',
            'contact_method' => 'Phone',
            'price' => 250,
            'post_type_id' => $this->postType['id']
        ]);

        $this->assertDatabaseHas('posts', [
            'title' => 'test title',
            'price' => 250
        ]);
    }

    public function testAddingNewPostWithExchangeDetailsSavesToDatabase()
    {
        $this->post('/api/posts', [
            'title' => 'test title',
            'description' => 'desc test',
            'image_url' => 'img1.jpg',
            'pickup_location' => 'Egypt',
            'contact_method' => 'Phone',
            'exchange_details' => 'exchange details example',
            'post_type_id' => $this->postType['id']
        ]);

        $this->assertDatabaseHas('posts', [
            'title' => 'test title',
            'exchange_details' => 'exchange details example'
        ]);
    }

    public function testAddingPostWithInvalidImageReturnError()
    {
        $response = $this->post('/api/posts/image', [
            'image' => 'img1.jpg',
        ]);

        $response->assertJsonPath('errors.image', ['This is not an image', 'The image must be a file of type: jpeg, png, jpg, gif, svg.']);
    }

    public function testAddingValidImageUploaded()
    {
        \Storage::fake('public_html');

        $response = $this->post('/api/posts/image', [
            'image' => $img = \Illuminate\Http\UploadedFile::fake()->image('img1.jpg'),
        ]);

        \Storage::disk('public_html')->assertExists('images/' . $img->hashName());
        \Storage::disk('public_html')->assertMissing('img2.jpg');
        $response->assertStatus(200)->assertJsonStructure(['image_url']);
    }
}
