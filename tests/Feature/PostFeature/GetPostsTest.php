<?php

namespace Tests\Feature\PostFeature;

use App\Models\PostType;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Models\Post;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class GetAllPostsTest extends TestCase
{
    use RefreshDatabase, WithoutMiddleware;
    private $postType;

    public function setUp(): void
    {
        parent::setUp();
        $this->seedUserAndPostTypes();
        $this->installPassportAndSetupUser();
        $this->postType = PostType::create(['name' => 'sell']);
    }

    public function testShouldReturnActivePosts()
    {
        $this->artisan('db:seed --class=PostSeeder');

        $response = $this->get('/api/posts');

        $response->assertOk();
        $response->assertJson(['total' => Post::status('ACTIVE')->count()]);
    }

    public function testShouldReturnPostsInreview()
    {
        $this->artisan('db:seed --class=PostSeeder');

        $response = $this->get('/api/posts?status=inreview');
        $response->assertOk();
        $response->assertJson(['total' => Post::status('INREVIEW')->count()]);
    }

    public function testShouldReturnRejectedPosts()
    {
        $this->artisan('db:seed --class=PostSeeder');

        $response = $this->get('/api/posts?status=rejected');
        $response->assertOk();
        $response->assertJson(['total' => Post::status('REJECTED')->count()]);
    }

    public function testShouldReturnSoldPosts()
    {
        $this->artisan('db:seed --class=PostSeeder');

        $response = $this->get('/api/posts?status=sold');
        $response->assertOk();
        $response->assertJson(['total' => Post::status('SOLD')->count()]);
    }

    public function testShouldReturnInvalidStatusPassed()
    {
        $this->artisan('db:seed --class=PostSeeder');

        $response = $this->get('/api/posts?status=abcd');
        $response->assertStatus(302);
    }
}
