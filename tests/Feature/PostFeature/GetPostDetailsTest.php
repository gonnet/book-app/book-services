<?php

namespace Tests\Feature\PostFeature;

use App\Models\PostType;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class GetPostDetailsTest extends TestCase
{
    use RefreshDatabase;
    private $postType;

    public function setUp(): void
    {
        parent::setUp();

        $this->seedUserAndPostTypes();
        $this->installPassportAndSetupUser();

        $this->postType = PostType::where(['name' => 'SELL'])->first();
    }

    public function testShouldReturnPostDetailsWhenPassingPostId()
    {
        $post = $this->post('/api/posts', [
            'title' => 'test title',
            'description' => 'desc test',
            'image_url' => 'img1.jpg',
            'pickup_location' => 'Egypt',
            'contact_method' => 'Phone',
            'user_id' => $this->user['id'],
            'post_type_id' => $this->postType['id']
        ]);
        $response = $this->get('/api/posts/' . $post['id']);

        $response->assertOk();
        $response->assertSee([
            'title' => 'test title',
            'description' => 'desc test',
            'image_url' => 'img1.jpg',
            'pickup_location' => 'Egypt',
            'contact_method' => 'Phone',
            'status' => 'INREVIEW',
            'user_id' => $this->user['id'],
            'post_type_id' => $this->postType['id'],
            'post_type_readable' => $this->postType['name']
        ]);
    }

    public function testShouldReturnNotFoundWhenRequestingNotExistingPost()
    {
        $response = $this->get('/api/posts/100');

        $response->assertNotFound();
    }

    public function testShouldReturnNotFoundWhenIdIsInvalid()
    {
        $response = $this->get('/api/posts/ab#');

        $response->assertStatus(404);
    }
}
