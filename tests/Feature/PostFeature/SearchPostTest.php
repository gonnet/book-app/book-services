<?php

namespace Tests\Feature\PostFeature;

use App\Models\Post;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class SearchPostTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->seedUserAndPostTypes();
        $this->installPassportAndSetupUser();

        Post::factory()->create(['title' => 'post title test', 'status' => 'ACTIVE']);
        Post::factory()->create(['description' => 'this is post two', 'status' => 'ACTIVE']);
        Post::factory()->create(['description' => 'this is post two in review', 'status' => 'INREVIEW']);
    }

    public function testSearchPostFindsPostByTitle()
    {
        $response = $this->get('/api/posts?search=title');

        $response->assertStatus(200);
        $response->assertJson(['total' => 1]);
    }

    public function testSearchPostFindsPostByDescription()
    {
        $response = $this->get('/api/posts?search=two');

        $response->assertStatus(200);
        $response->assertJson(['total' => 1]);
    }

    public function testSearchInReviewPostsShouldWork()
    {
        $response = $this->get('/api/posts?search=two&status=inreview');

        $response->assertStatus(200);
        $response->assertJson(['total' => 1]);
    }

    public function testSearchRejectedPostsShouldWork()
    {
        $response = $this->get('/api/posts?search=two&status=rejected');

        $response->assertStatus(200);
        $response->assertJson(['total' => 0]);
    }
}
