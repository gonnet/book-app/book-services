<?php

namespace Tests\Feature\PostFeature;

use App\Models\PostType;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class GetUserPostsTest extends TestCase
{
    use RefreshDatabase;
    private $postType;

    public function setUp(): void
    {
        parent::setUp();

        $this->seedUserAndPostTypes();
        $this->installPassportAndSetupUser();
        
        $this->postType = $this->post('/api/post-types', ['name' => 'SELL']);
    }

    public function testShouldReturnUserPostsWhenProvidingSpecficUser()
    {
        $this->post('/api/posts', [
            'title' => 'test title',
            'description' => 'desc test',
            'image_url' => 'img1.jpg',
            'pickup_location' => 'Egypt',
            'contact_method' => 'Phone',
            'status' => 'ACTIVE',
            'post_type_id' => $this->postType['id']
        ]);

        $this->post('/api/posts', [
            'title' => 'test title2',
            'description' => 'desc test',
            'image_url' => 'img1.jpg',
            'pickup_location' => 'Egypt',
            'contact_method' => 'Phone',
            'status' => 'ACTIVE',
            'post_type_id' => $this->postType['id']
        ]);

        $response = $this->get('/api/users/posts');
        $response->assertOk();
        $response->assertSee('test title2');
        $response->assertSee(['total' => 2]);
    }
    
    public function testShouldReturnUserPostsEmptyWhenUserHasNoPosts()
    {
        $response = $this->get('/api/users/posts');
        $response->assertOk();
        $response->assertSee(['total' => 0]);
    }
}
