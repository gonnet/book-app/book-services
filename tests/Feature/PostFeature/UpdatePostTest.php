<?php

namespace Tests\Feature\PostFeature;

use App\Models\PostType;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UpdatePostTest extends TestCase
{
    use RefreshDatabase;
    private $sellPostType;
    private $exchangePostType;

    public function setUp(): void
    {
        parent::setUp();

        $this->seedUserAndPostTypes();
        $this->installPassportAndSetupUser();
        
        $this->postTypeSell = PostType::where(['name' => 'SELL'])->first();
        $this->postTypeExchange = PostType::where(['name' => 'EXCHANGE'])->first();

        $this->adminUser = $this->createAdminUser();
    }

    public function testShouldUpdatePostStatus()
    {
        $post = $this->post('/api/posts', [
            'title' => 'test title',
            'description' => 'desc test',
            'image_url' => 'img1.jpg',
            'pickup_location' => 'Egypt',
            'contact_method' => 'Phone',
            'user_id' => $this->user['id'],
            'post_type_id' => $this->postTypeSell['id']
        ]);

        $response = $this->actingAs($this->adminUser)->patch('/api/posts/' . $post['id'] . '/status', ['status' => 'INREVIEW']);
        $response->assertOk();
        $this->assertDatabaseHas('posts', ['id' => $post['id'], 'status' => 'INREVIEW']);
    }

    public function testShouldReturnInvalidWhenUpdatingPostStatusIsNotCorrect()
    {
        $post = $this->post('/api/posts', [
            'title' => 'test title',
            'description' => 'desc test',
            'image_url' => 'img1.jpg',
            'pickup_location' => 'Egypt',
            'contact_method' => 'Phone',
            'user_id' => $this->user['id'],
            'post_type_id' => $this->postTypeSell['id']
        ]);

        $response = $this->actingAs($this->adminUser)->patch('/api/posts/' . $post['id'] . '/status', ['status' => 'ABC']);
        $response->assertJsonPath('errors.status', ['The selected status is invalid.']);
    }

    public function testShouldUpdatePost()
    {
        $post = $this->post('/api/posts', [
            'title' => 'test title',
            'description' => 'desc test',
            'image_url' => 'img1.jpg',
            'pickup_location' => 'Egypt',
            'contact_method' => 'Phone',
            'post_type_id' => $this->postTypeSell['id']
        ]);

        $response = $this->put('/api/posts/' . $post['id'], [
            'title' => 'test title updated',
            'description' => 'desc test updated',
            'image_url' => 'img1.jpg.updated',
            'pickup_location' => 'Egypt updated',
            'contact_method' => 'PhoneUpdated',
            'price' => null,
            'exchange_details' => 'Exchange Details test',

        ]);

        $response->assertOk();
        $this->assertDatabaseHas('posts', [
            'id' => $post['id'],
            'title' => 'test title updated',
            'description' => 'desc test updated',
            'image_url' => 'img1.jpg.updated',
            'pickup_location' => 'Egypt updated',
            'contact_method' => 'PhoneUpdated',
            'price' => null,
            'exchange_details' => 'Exchange Details test',
        ]);
    }

    public function testShouldReturnErrorWhenUpdaterIsNotTheAuthor()
    {
        $post = $this->post('/api/posts', [
            'title' => 'test title',
            'description' => 'desc test',
            'image_url' => 'img1.jpg',
            'pickup_location' => 'Egypt',
            'contact_method' => 'Phone',
            'post_type_id' => $this->postTypeSell['id']
        ]);

        $response = $this->actingAs(User::factory()->create())->put('/api/posts/' . $post['id'], [
            'title' => 'test title updated',
            'description' => 'desc test updated',
            'image_url' => 'img1.jpg.updated',
            'pickup_location' => 'Egypt updated',
            'contact_method' => 'PhoneUpdated',
            'post_type_id' => $this->postTypeExchange['id'],
            'price' => null,
            'exchange_details' => 'Exchange Details test',

        ]);

        $response->assertForbidden();
        $response->assertJsonPath('message', 'This action is unauthorized.');
    }

    public function testShouldReturnForbiddenWhenPostStatusIsActive()
    {
        $post = $this->post('/api/posts', [
            'title' => 'test title',
            'description' => 'desc test',
            'image_url' => 'img1.jpg',
            'pickup_location' => 'Egypt',
            'contact_method' => 'Phone',
            'post_type_id' => $this->postTypeSell['id']
        ]);

        $response = $this->actingAs($this->adminUser)->patch('/api/posts/' . $post['id'] . '/status', ['status' => 'ACTIVE']);
        $response = $this->put('/api/posts/' . $post['id'], [
            'title' => 'test title updated',
            'description' => 'desc test updated',
            'image_url' => 'img1.jpg.updated',
            'pickup_location' => 'Egypt updated',
            'contact_method' => 'PhoneUpdated',
            'post_type_id' => $this->postTypeExchange['id'],
            'price' => null,
            'exchange_details' => 'Exchange Details test',
        ]);

        $response->assertForbidden();
        $response->assertJsonPath('message', 'This action is unauthorized.');
    }

    public function testShouldReturnForbiddenWhenPostStatusIsSold()
    {
        $post = $this->post('/api/posts', [
            'title' => 'test title',
            'description' => 'desc test',
            'image_url' => 'img1.jpg',
            'pickup_location' => 'Egypt',
            'contact_method' => 'Phone',
            'post_type_id' => $this->postTypeSell['id']
        ]);

        $this->actingAs($this->adminUser)->patch('/api/posts/' . $post['id'] . '/status', ['status' => 'SOLD']);
        $response = $this->actingAs($this->adminUser)->patch('/api/posts/' . $post['id'] . '/status', ['status' => 'ACTIVE']);

        $response->assertForbidden();
        $response->assertJsonPath('message', "Cannot update sold post");
    }
}
