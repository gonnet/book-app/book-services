<?php

namespace Tests\Feature\PostFeature;

use App\Models\Post;
use App\Models\PostType;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class DeletePostTest extends TestCase
{
    use RefreshDatabase;
    private $postType;

    public function setUp(): void
    {
        parent::setUp();

        $this->seedUserAndPostTypes();
        $this->installPassportAndSetupUser();
        
        $this->postType = $this->post('/api/post-types', ['name' => 'SELL']);
    }

    public function testShouldReturnOkAndDeletePost()
    {
        $post = $this->post('/api/posts', [
            'title' => 'test title',
            'description' => 'desc test',
            'image_url' => 'test.png',
            'pickup_location' => 'Egypt',
            'contact_method' => 'Phone',
            'status' => 'ACTIVE',
            'user_id' => $this->user['id'],
            'post_type_id' => $this->postType['id']
        ]);
        $response = $this->delete('/api/posts/' . $post['id']);
        $this->assertDatabaseCount('posts', 0);
        $response->assertOk();
    }
}
