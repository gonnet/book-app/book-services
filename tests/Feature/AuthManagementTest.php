<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Passport\Passport;
use Tests\TestCase;

class AuthManagementTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
        $this->seedUserAndPostTypes();
        $this->installPassportAndSetupUser();

    }
    
    public function testRegisterReturnsToken()
    {
        $response = $this->post('/api/register', [
            'name' => 'Usef',
            'email' => 'abc@xyz.com',
            'password' => '123123123'
        ]);

        $response->assertStatus(201)->assertJsonStructure(['token']);
    }

    public function testLoginWithValidUserReturnsToken()
    {
        $this->post('/api/register', [
            'name' => 'Usef',
            'email' => 'abc@xyz.com',
            'password' => '123123123'
        ]);

        $response = $this->post('/api/login', [
            'email' => 'abc@xyz.com',
            'password' => '123123123'
        ]);

        $response->assertStatus(200)->assertJsonStructure(['token']);
    }

    public function testLogoutSuccessfulWithValidUser()
    {
        $this->post('/api/register', [
            'name' => 'Usef',
            'email' => 'abc@xyz.com',
            'password' => '123123123'
        ]);

        $response = $this->post('/api/login', [
            'email' => 'abc@xyz.com',
            'password' => '123123123'
        ]);

        $token = $response['token'];
        $this->withHeaders(['Authorization' => "Bearer $token"]);
        $response = $this->post('/api/logout');
        $response->assertOk();
    }

    public function testLogoutUnauthorizedWithoutToken()
    {
        $this->withHeaders(['Authorization' => '']);
        $response = $this->post('/api/logout');
        $response->assertUnauthorized();
    }
}
