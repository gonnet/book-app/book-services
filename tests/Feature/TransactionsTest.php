<?php

namespace Tests\Feature;

use App\Models\Post;
use App\Models\PostType;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class TransactionsTest extends TestCase
{
    use RefreshDatabase;
    private $user2;
    private $postType;
    private $post;

    public function setUp(): void
    {
        parent::setUp();
        $this->seedUserAndPostTypes();
        $this->installPassportAndSetupUser();
        $this->postType = PostType::first();
        $this->post = $this->post('/api/posts', [
            'title' => 'test title',
            'description' => 'desc test',
            'image_url' => 'img1.jpg',
            'pickup_location' => 'Egypt',
            'contact_method' => 'Phone',
            'post_type_id' => $this->postType['id']
        ]);
    }

    public function testCreateTransactionReturn201AndCode()
    {
        $transaction = ['post_id' => $this->post['id']];
        $response = $this->actingAs(User::factory()->create())->post('/api/transactions', $transaction);


        $response->assertStatus(201);
        $response->assertJsonStructure(['code', 'post_id', 'buyer_id', 'seller_id']);
    }

    public function testCannotCreateTransactionIfImPostOwner()
    {
        $post = $this->post('/api/posts', [
            'title' => 'test title',
            'description' => 'desc test',
            'image_url' => 'img1.jpg',
            'pickup_location' => 'Egypt',
            'contact_method' => 'Phone',
            'user_id' => $this->user['id'],
            'post_type_id' => $this->postType['id']
        ]);
        $transaction = ['post_id' => $post['id']];
        $response = $this->post('/api/transactions', $transaction);

        $response->assertStatus(400);
    }

    public function testConfirmingTransactionReturns200IfValid()
    {
        $transactionRes = $this->actingAs(User::factory()->create())->post('/api/transactions', ['post_id' => $this->post['id']]);

        $transaction = [
            'transaction_id' => $transactionRes['id'],
            'code' => $transactionRes['code']
        ];

        Post::where('id', $this->post['id'])->update(['status' => 'ACTIVE']);

        $response = $this->actingAs(User::first())->post('/api/transactions/confirm', $transaction);
        $response->assertStatus(200);
        $response->assertJson(['confirmed' => true]);
    }

    public function testConfirmingTransactionReturn422IfInvalidTransactionId()
    {
        $transaction = [
            'transaction_id' => 102,
            'code' => 'd12hd2'
        ];

        $response = $this->post('/api/transactions/confirm', $transaction);
        $response->assertStatus(422);
    }

    public function testConfirmingTransactionReturn400IfCodeIsWrong()
    {
        $trans = $this->actingAs(User::factory()->create())->post('/api/transactions', ['post_id' => $this->post['id']]);
        $transaction = [
            'transaction_id' => $trans['id'],
            'code' => 'd12hd2'
        ];
        Post::where('id', $this->post['id'])->update(['status' => 'ACTIVE']);
        $response = $this->actingAs(User::first())->post('/api/transactions/confirm', $transaction);
        $response->assertStatus(400);
        $response->assertSee('Invalid code.');
    }

    public function testCannotConfirmTransactionIfNotPostOwner()
    {
        $trans = $this->actingAs(User::factory()->create())->post('/api/transactions', ['post_id' => $this->post['id']]);
        $transaction = [
            'transaction_id' => $trans['id'],
            'code' => $trans['code']
        ];

        Post::where('id', $this->post['id'])->update(['status' => 'ACTIVE']);

        $response = $this->post('/api/transactions/confirm', $transaction);
        $response->assertStatus(400);
        $response->assertSeeText("You cannot confirm another user");
    }

    public function testDoNotCreateNewTransactionForSameProductAndSameUsers()
    {
        $user = User::factory()->create();
        $transaction = ['post_id' => $this->post['id']];
        $response = $this->actingAs($user)->post('/api/transactions', $transaction);

        $response->assertStatus(201);
        $transactionId = $response['id'];

        $response = $this->actingAs($user)->post('/api/transactions', $transaction);
        $response->assertJson(['id' => $transactionId]);
    }

    public function testCannotConfirmTransactionOfAnyItemNotActive()
    {
        $post = $this->post('/api/posts', [
            'title' => 'test title',
            'description' => 'desc test',
            'image_url' => 'img1.jpg',
            'pickup_location' => 'Egypt',
            'contact_method' => 'Phone',
            'post_type_id' => $this->postType['id']
        ]);

        $transaction = ['post_id' => $post['id']];
        $response = $this->actingAs(User::factory()->create())->post('/api/transactions', $transaction);

        $response->assertStatus(201);
        $transactionResult = ['transaction_id' => $response['id'], 'code' => $response['code']];
        $response = $this->actingAs(User::first())->post('/api/transactions/confirm', $transactionResult);
        $response->assertStatus(400);
        $response->assertSee('You cannot confirm a post that is already sold or in review.');
    }

    public function testPostStatusUpdatedAfterConfirmingTransaction()
    {
        $transactionRes = $this->actingAs(User::factory()->create())->post('/api/transactions', ['post_id' => $this->post['id']]);

        $transaction = [
            'transaction_id' => $transactionRes['id'],
            'code' => $transactionRes['code']
        ];

        Post::where('id', $this->post['id'])->update(['status' => 'ACTIVE']);

        $response = $this->actingAs(User::first())->post('/api/transactions/confirm', $transaction);
        $response->assertStatus(200);
        $response->assertJson(['confirmed' => true]);

        $response = $this->get('/api/posts/'.$this->post['id']);
        $response->assertJson(['status' => 'SOLD']);
    }

    public function testNotificationsAreSentOutToBuyerAndSellerThatTheirTransactionIsSuccessful()
    {
        $transactionRes = $this->actingAs(User::factory()->create())->post('/api/transactions', ['post_id' => $this->post['id']]);

        $transaction = [
            'transaction_id' => $transactionRes['id'],
            'code' => $transactionRes['code']
        ];

        Post::where('id', $this->post['id'])->update(['status' => 'ACTIVE']);

        $response = $this->actingAs(User::first())->post('/api/transactions/confirm', $transaction);
        $response->assertStatus(200);
        $response->assertJson(['confirmed' => true]);

        $this->assertDatabaseHas('notifications', [
            'user_id' => $transactionRes['buyer_id'],
            'title' => 'Your transaction was successful.',
            'body' => 'Your transaction was successfully completed.',
        ]);

        $this->assertDatabaseHas('notifications', [
            'user_id' => $transactionRes['seller_id'],
            'title' => 'Your transaction was successful.',
            'body' => 'Your transaction was successfully completed.',
        ]);
    }

    public function testCanListAllOfUserTransactions()
    {
        $transactionRes = $this->actingAs(User::factory()->create())->post('/api/transactions', ['post_id' => $this->post['id']]);

        $transaction = [
            'transaction_id' => $transactionRes['id'],
            'code' => $transactionRes['code']
        ];

        Post::where('id', $this->post['id'])->update(['status' => 'ACTIVE']);

        $response = $this->actingAs(User::first())->post('/api/transactions/confirm', $transaction);
        $response->assertStatus(200);
        $response->assertJson(['confirmed' => true]);

        $response = $this->actingAs(User::first())->get('/api/users/transactions');
        $response->assertStatus(200)->assertJsonFragment(['post_id' => $this->post['id']]);
    }
}
