<?php

namespace Tests\Feature;

use App\Models\Post;
use App\Models\PostType;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PointSystemTest extends TestCase
{
    use RefreshDatabase;
    private $postType;
    private $post;
    
    public function setUp(): void
    {
        parent::setUp();
        $this->seedUserAndPostTypes();
        $this->installPassportAndSetupUser();
        $this->postType = PostType::where('name', 'SELL')->first();
        $this->post = $this->post('/api/posts', [
            'title' => 'test title',
            'description' => 'desc test',
            'image_url' => 'img1.jpg',
            'pickup_location' => 'Egypt',
            'contact_method' => 'Phone',
            'post_type_id' => $this->postType->id
        ]);
    }

    public function testSellerGetsPointsAfterTransactionIsConfirmed()
    {
        $transactionRes = $this->actingAs(User::factory()->create())->post('/api/transactions', ['post_id' => $this->post['id']]);

        $transaction = [
            'transaction_id' => $transactionRes['id'],
            'code' => $transactionRes['code']
        ];

        Post::where('id', $this->post['id'])->update(['status' => 'ACTIVE']);

        $response = $this->actingAs(User::first())->post('/api/transactions/confirm', $transaction);
        $response->assertStatus(200);
        $response->assertJson(['confirmed' => true]);

        $this->assertDatabaseHas('points_logs', [
            'transaction_id' => $transactionRes['id'],
            'user_id' => $transactionRes['seller_id'],
            'points_earned' => $this->postType->seller_points
        ]);

        $this->assertDatabaseHas('users', [
            'id' => $transactionRes['seller_id'],
            'points' => $this->postType->seller_points
        ]);
    }

    public function testBuyerGetsPointsAfterTransactionIsConfirmed()
    {
        $user = User::factory()->create();
        $transactionRes = $this->actingAs($user)->post('/api/transactions', ['post_id' => $this->post['id']]);

        $transaction = [
            'transaction_id' => $transactionRes['id'],
            'code' => $transactionRes['code']
        ];

        Post::where('id', $this->post['id'])->update(['status' => 'ACTIVE']);

        $response = $this->actingAs(User::first())->post('/api/transactions/confirm', $transaction);
        $response->assertStatus(200);
        $response->assertJson(['confirmed' => true]);

        $this->assertDatabaseHas('points_logs', [
            'transaction_id' => $transactionRes['id'],
            'user_id' => $transactionRes['buyer_id'],
            'points_earned' => $this->postType->buyer_points
        ]);

        $this->assertDatabaseHas('users', [
            'id' => $transactionRes['buyer_id'],
            'points' => $this->postType->buyer_points
        ]);
    }
}
