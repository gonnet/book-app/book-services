<?php

namespace Tests\Feature\UserFeature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UserSignUpTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
        $this->seedUserAndPostTypes();
        $this->installPassportAndSetupUser();
    }

    public function testResponse201WhenNewUserCreated()
    {
        $response = $this->post('/api/register', [
            'name' => 'ashraf',
            'email' => 'ashraf@gmail.com',
            'password' => '123123123'
        ]);

        $response->assertStatus(201);
    }

    public function testUserDataIsStoredAfterSignup()
    {
        $response = $this->post('/api/register', [
            'name' => 'Ashraf',
            'email' => 'ashraf@gmail.com',
            'password' => '123123123'
        ]);

        $this->assertDatabaseHas('users', [
            'email' => 'ashraf@gmail.com',
        ]);
    }

    public function testValidationErrorWhenGivenInvalidEmail()
    {
        $response = $this->post('/api/register', [
            'name' => 'Ashraf',
            'email' => 'amail.com',
            'password' => '123123123'
        ]);

        $response->assertJsonPath('errors.email', ['The email must be a valid email address.']);
    }

    public function testValidationErrorWhenGivenInvalidName()
    {
        $response = $this->post('/api/register', [
            'name' => '',
            'email' => 'ashraf@gmail.com',
            'password' => '123123123'
        ]);

        $response->assertJsonPath('errors.name', ['Please enter a valid name']);
    }

    public function testValidationErrorWhenGivenInvalidPassword()
    {
        $response = $this->post('/api/register', [
            'name' => 'Ashref',
            'email' => 'ashraf@gmail.com',
            'password' => '123'
        ]);

        $response->assertJsonPath('errors.password', ['The password must be at least 8 characters.']);
    }

    public function testCannotAddExistingEmail()
    {
        $response = $this->post('/api/register', [
            'name' => 'Ashref',
            'email' => 'ashraf@gmail.com',
            'password' => '123123123'
        ]);

        $response = $this->post('/api/register', [
            'name' => 'Ashref',
            'email' => 'ashraf@gmail.com',
            'password' => '123123123'
        ]);

        $response->assertStatus(422)->assertJsonPath('errors.email', ['User exists']);
    }

    public function testShouldRegisterAndReturnPhoneDataWhenRegisterWithProvidingPhoneNumber()
    {
        $response = $this->post('/api/register', [
            'name' => 'Ashraf',
            'email' => 'ashraf@gmail.com',
            'password' => '123123123',
            'phone' => '01022644579',
        ]);

        $this->assertDatabaseHas('users', [
            'phone' => '01022644579',
        ]);

        $response->assertStatus(201);
        $response->assertSee(['phone' => '01022644579']);
    }
}
