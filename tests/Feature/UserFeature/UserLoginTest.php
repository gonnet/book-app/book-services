<?php

namespace Tests\Feature\UserFeature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UserLoginTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
        $this->seedUserAndPostTypes();
        $this->installPassportAndSetupUser();
    }
    
    public function testUserLoginSucessfullyReturned200()
    {
        $this->post('/api/register', [
            'name' => 'ashraf',
            'email' => 'ahmed.ashref.sw@gmail.com',
            'password' => '12345678'
        ]);
        $response = $this->post("/api/login", [
            "email" => "ahmed.ashref.sw@gmail.com",
            "password" => "12345678",
        ]);

        $response->assertStatus(200);
    }

    public function testUserLoginNotFound()
    {
        $response = $this->post("/api/login", [
            "email" => "ahmed.ashref.sw@gmail.com",
            "password" => "12345678",
        ]);

        $response->assertStatus(422)->assertJsonPath('errors.email', ['This email does not exist.']);
    }

    public function testUserLoginWrongPassword()
    {
        $this->post('/api/register', [
            'name' => 'ashraf',
            'email' => 'ahmed.ashref.sw@gmail.com',
            'password' => '12345678'
        ]);
        $response = $this->post("/api/login", [
            "email" => "ahmed.ashref.sw@gmail.com",
            "password" => "123456899",
        ]);

        $response->assertStatus(422)->assertSee("Password mismatch");
    }

    public function testValidationErrorWhenGivenInvalidEmail()
    {
        $response = $this->post('/api/login', [
            'email' => 'amail.com',
            'password' => '123123123'
        ]);

        $response->assertJsonPath('errors.email', ['The email must be a valid email address.']);
    }

    public function testValidationErrorWhenGivenInvalidPassword()
    {
        $response = $this->post('/api/login', [
            'email' => 'ashraf@gmail.com',
            'password' => '12'
        ]);

        $response->assertJsonPath('errors.password', ['The password must be at least 5 characters.']);
    }

    public function testShouldReturnPhoneWhenUserHasPhoneDataSavedInTheSystem()
    {
        $this->post('/api/register', [
            'name' => 'ashraf',
            'email' => 'ahmed.ashref.sw@gmail.com',
            'password' => '12345678',
            'phone' => '01022644578'
        ]);
        $response = $this->post("/api/login", [
            "email" => "ahmed.ashref.sw@gmail.com",
            "password" => "12345678",
        ]);

        $response->assertStatus(200);
        $response->assertSee(['phone' => "01022644578",]);
    }

    public function testShouldReturnApiTokenWhenUserLogsIn()
    {
        $this->post('/api/register', [
            'name' => 'ashraf',
            'email' => 'ahmed.ashref.sw@gmail.com',
            'password' => '12345678',
            'phone' => '01022644578'
        ]);
        $response = $this->post("/api/login", [
            "email" => "ahmed.ashref.sw@gmail.com",
            "password" => "12345678",
        ]);

        $response->assertOk()->assertJsonStructure(['token']);
    }
}
