<?php

namespace Tests\Feature\UserFeature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ForgotPasswordTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
        $this->seedUserAndPostTypes();
        $this->installPassportAndSetupUser();
    }
    
    public function testUserCanSendRequestToResetPasswordAndGetCode()
    {
        $response = $this->post('/api/forgot-password', [
            'email' => $this->user['email'],
        ]);

        $response->assertStatus(200);
        $response->assertJsonStructure(['code']);
    }

    public function testInvalidEmailReturnSuitableError()
    {
        $response = $this->post('/api/forgot-password', [
            'email' => 'ahmed.ashref.sw@gmail.com',
        ]);

        $response->assertStatus(422);
        $response->assertJson(['message' => 'The selected email is invalid.']);
    }

    public function testCanResetPassword()
    {
        $response = $this->post('/api/forgot-password', [
            'email' => $this->user['email'],
        ]);

        $code = $response['code'];

        $response = $this->post('/api/reset-password', [
            'code' => $code,
            'email' => $this->user['email'],
            'password' => '12345678',
            'password_confirmation' => '12345678'
        ]);

        $response->assertStatus(200);

        $response = $this->post('/api/login', [
            'email' => $this->user['email'], 
            'password' => '12345678'
        ]);
        $response->assertStatus(200);
    }

    public function testPasswordConfirmationShouldBeSame(){
        $response = $this->post('/api/forgot-password', [
            'email' => $this->user['email'],
        ]);

        $code = $response['code'];

        $response = $this->post('/api/reset-password', [
            'code' => $code,
            'email' => $this->user['email'],
            'password' => '12345678',
            'password_confirmation' => '123456789'
        ]);

        $response->assertStatus(422);
        $response->assertSee('The password confirmation does not match.');
    }

    public function testCheckExpiredCode(){
        $response = $this->post('/api/forgot-password', [
            'email' => $this->user['email'],
        ]);

        $code = $response['code'];

        $this->travel(1)->hours();
        
        $response = $this->post('/api/reset-password', [
            'code' => $code,
            'email' => $this->user['email'],
            'password' => '12345678',
            'password_confirmation' => '12345678'
        ]);

        $response->assertStatus(400);
        $response->assertSee('Code expired.');
    }
}
