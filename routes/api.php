<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\LeaderboardController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\PostTypeController;
use App\Http\Controllers\TransactionController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::controller(AuthController::class)->group(function () {
    Route::post('/login', 'login');
    Route::post('/register', 'register');
    Route::post('/forgot-password', 'forgotPassword');
    Route::post('/reset-password', 'resetPassword');
});

Route::middleware('auth:api')->group(function () {
    Route::get('/user', [UserController::class, 'user']);

    Route::post('/logout', [AuthController::class, 'logout']);

    Route::controller(LeaderboardController::class)->prefix('leaderboard')->as('leaderboard.')->group(function () {
        Route::get('/', 'index');
    });

    Route::controller(PostController::class)->prefix('posts')->as('posts.')->group(
        function () {
            Route::get('/', 'index');
            Route::post('/', 'store');
            Route::post('/image', 'uploadImage');
            Route::get('/{post}', 'show')->name('show');
            Route::delete('/{post}', 'destroy')->name('destroy');
            Route::put('/{post}', 'update')->name('update');
            Route::patch('/{post}/status', 'updateStatus');
        }
    );

    Route::controller(PostTypeController::class)->prefix('post-types')->as('postTypes.')->group(
        function () {
            Route::get('/', 'index');
            // TODO: change those so only admin can store or delete post types
            Route::post('/', 'store');
            Route::delete('/{postType}', 'destroy')->name('destroy');
        }
    );

    Route::controller(UserController::class)->prefix('users')->as('users.')->group(function () {
        Route::post('/setFcmToken', 'setFcmToken');
        Route::post('/deleteFcmToken', 'deleteFcmToken');
        Route::get('/posts', 'posts')->name('posts.index');
        Route::get('/notifications', 'notifications')->name('notifications.index');
        Route::get('/transactions', 'transactions')->name('transactions.index');
    });

    Route::controller(NotificationController::class)->prefix('notifications')->as('notifications.')->group(function () {
        Route::post('/send', 'sendPushNotification');
    });

    Route::controller(TransactionController::class)->prefix('transactions')->as('transactions.')->group(function () {
        Route::post('/', 'store');
        Route::post('/confirm', 'confirm');
    });
});
