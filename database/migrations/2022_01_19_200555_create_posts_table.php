<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->text('description');
            $table->string('image_url');
            $table->integer('number_of_papers')->nullable();
            $table->string('pickup_location');
            $table->string('contact_method');
            $table->double('price')->nullable();
            $table->text('exchange_details')->nullable();
            $table->enum('status', ['ACTIVE', 'REJECTED', 'INREVIEW', 'SOLD']);
            $table->timestamps();

            //TODO:: add type field and make it required.
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
