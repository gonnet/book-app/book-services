<?php

namespace Database\Seeders;

use DB;
use Illuminate\Database\Seeder;

class UserTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_types')->insert([
            [
                'id' => 1,
                'name' => 'Admin',
                'active' => 1
            ], [
                'id' => 2,
                'name' => 'User',
                'active' => 1
            ]
        ]);
    }
}
