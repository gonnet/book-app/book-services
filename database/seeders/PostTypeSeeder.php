<?php

namespace Database\Seeders;

use App\Models\PostType;
use DB;
use Illuminate\Database\Seeder;

class PostTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('post_types')->insert([
            [
                'id' => 1,
                'name' => 'SELL',
                'buyer_points' => 30,
                'seller_points' => 10,
            ], [
                'id' => 2,
                'name' => 'EXCHANGE',
                'buyer_points' => 20,
                'seller_points' => 20,
            ], [
                'id' => 3,
                'name' => 'DONATE',
                'buyer_points' => 10,
                'seller_points' => 30,
            ]
        ]);
    }
}
