<?php

namespace Database\Factories;

use App\Models\PostType;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class PostFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $users = User::pluck('id')->toArray();
        $status = ['ACTIVE', 'INREVIEW', 'REJECTED', 'SOLD'];
        $postTypes = PostType::pluck('id')->toArray();
        return [
            'user_id' => $this->faker->randomElement($users),
            'title' => $this->faker->name(),
            'description' => $this->faker->text(500),
            'image_url' => $this->faker->url(),
            'price' => $this->faker->randomDigit(),
            'pickup_location' => $this->faker->text(),
            'contact_method' => $this->faker->text(),
            'status' => $this->faker->randomElement($status),
            'post_type_id' => $this->faker->randomElement($postTypes)
        ];
    }
}
