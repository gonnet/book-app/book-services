<?php

namespace App\Http\Requests;

use App\Models\Post;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:5',
            'description' => 'required',
            'image_url' => 'required',
            'pickup_location' => 'required',
            'contact_method' => 'required',
            'post_type_id' => 'required|exists:post_types,id',
            'price' => 'nullable',
            'exchange_details' => 'nullable',
        ];
    }


    public function messages()
    {
        return [
            'title.required' => 'Title is required',
            'title.min' => 'Title must be at least of length 5',
            'description.required' => 'Description is required',
            'image_url.required' => 'Image is required',
            'pickup_location.required' => 'Location is required',
            'contact_method.required' => 'Contact method is required',
            'post_type_id.required' => 'post type id is required',
            'post_type_id.exists' => 'post type id does not exist',
        ];
    }
}
