<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserLoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email:rfc,dns|exists:users',
            'password' => 'required|min:5'
        ];
    }

    public function messages()
    {
        return [
            'email.email' => 'The email must be a valid email address.',
            'password.min' => 'The password must be at least 5 characters.',
            'email.exists' => 'This email does not exist.'
        ];
    }
}
