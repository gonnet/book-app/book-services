<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('update-post', $this->post);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'nullable',
            'description' => 'nullable',
            'image_url' => 'nullable',
            'pickup_location' => 'nullable',
            'contact_method' => 'nullable',
            'price' => 'nullable',
            'exchange_details' => 'nullable',
        ];
    }
}
