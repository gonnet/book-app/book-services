<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email:rfc,dns|unique:users',
            'name' => 'required|min:2',
            'password' => 'required|min:8',
            'phone' => 'min:8|unique:users,phone'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Please enter a valid name',
            'name.min' => 'Please enter a valid name',
            'email.email' => 'The email must be a valid email address.',
            'password.min' => 'The password must be at least 8 characters.',
            'phone.min' => 'The phone number must be at least 8 digits',
            // 'phone.exists' => 'The phone number exists , must be unique per account',
            'email.unique' => 'User exists',
            'phone.unique' => 'Phone should be unique',
        ];
    }
}
