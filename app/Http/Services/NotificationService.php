<?php

namespace App\Http\Services;

use App\Models\User;
use Http;

class NotificationService
{
    public static function sendPushNotification($users, $title, $body, $details)
    {
        $tokens = User::whereIn('id', $users)->with('fcmTokens')->get()->pluck('fcmTokens.*.token')->collapse();
        if ($tokens->count() == 0) {
            static::storeNotification($users, $title, $body, $details);
            return false;
        }

        $SERVER_API_KEY = config('firebase.FCM_SERVER_KEY');

        $data = [
            "registration_ids" => $tokens,
            "notification" => [
                "title" => $title,
                "body" => $body,
                "sound" => "default",
            ]
        ];

        $response = Http::withHeaders([
                'Authorization' => 'key=' . $SERVER_API_KEY,
                'Content-Type' => 'application/json',
                ])->post('https://fcm.googleapis.com/fcm/send', $data);
                
        if ($response->successful()) {
            $notifications = static::storeNotification($users, $title, $body, $details);
            return $notifications;
        }
        return false;
    }

    private static function storeNotification($users, $title, $body, $details)
    {
        $users = User::whereIn('id', $users)->get();
        $notifications = $users->map(function ($user) use ($title, $body, $details) {
            return $user->notifications()->create([
                    'title' => $title,
                    'body' => $body,
                    'details' => $details
                ]);
        });
        return $notifications;
    }
}
