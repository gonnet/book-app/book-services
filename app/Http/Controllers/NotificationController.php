<?php

namespace App\Http\Controllers;

use App\Http\Services\NotificationService;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    public function sendPushNotification(Request $request)
    {
        $result = NotificationService::sendPushNotification($request->users, $request->title, $request->body, $request->details);
        if ($result) {
            return response($result, 200);
        }
        return response(['error' => 'An error has occurred.'], 400);
    }
}
