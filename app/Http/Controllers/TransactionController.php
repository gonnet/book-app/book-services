<?php

namespace App\Http\Controllers;

use App\Http\Requests\ConfirmTransactionRequest;
use App\Http\Requests\StoreTransactionRequest;
use App\Models\Post;
use App\Models\Transaction;

class TransactionController extends Controller
{
    public function store(StoreTransactionRequest $request)
    {
        $transaction = $request->validated();
        $user_id = $request->user()->id;
        $transaction['buyer_id'] = $user_id;
        $transaction['seller_id'] = Post::find($transaction['post_id'])->user_id;
        $transaction['confirmed'] = false;

        if ($user_id == $transaction['seller_id']) {
            return abort(400, 'You cannot sell to your self.');
        }

        $res = Transaction::firstOrCreate($transaction);
        if ($res) {
            return response($res, 201);
        }
        return response('Error', 400);
    }

    public function confirm(ConfirmTransactionRequest $request)
    {
        $req = $request->validated();
        $userId = $request->user()->id;
        $transaction = Transaction::find($req['transaction_id']);

        if (!$transaction) {
            abort(400);
        }

        $postStatusActive = $transaction->post->status === 'ACTIVE';
        
        if ($userId !== $transaction['seller_id']) {
            return abort(400, 'You cannot confirm another user\'s post.');
        }

        if (!$postStatusActive) {
            return abort(400, 'You cannot confirm a post that is already sold or in review.');
        }

        if ($transaction->code !== $req['code']) {
            return abort(400, 'Invalid code.');
        }
        
        $transaction->update(['confirmed' => true]);

        return response(['confirmed' => $transaction->confirmed], 200);
    }
}
