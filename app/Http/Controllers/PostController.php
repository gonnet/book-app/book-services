<?php

namespace App\Http\Controllers;

use App\Http\Requests\GetPostsRequest;
use App\Http\Requests\ImageUploadRequest;
use App\Http\Requests\PostRequest;
use App\Http\Requests\PostStatusRequest;
use App\Http\Requests\UpdatePostRequest;
use App\Models\Post;
use Cloudinary\Cloudinary;
use Cloudinary\Configuration\Configuration;

class PostController extends Controller
{
    public function index(GetPostsRequest $request)
    {
        $status = $request->status ?? 'ACTIVE';
        $search = $request->search ?? null;
        
        if ($search) {
            return Post::search($search)->query(function ($q) use ($status){
                return $q->status($status)->latest();
            })->paginate(10);
        }

        return response(Post::status($status)->latest()->paginate(10), 200);
    }

    public function store(PostRequest $request)
    {
        $post = $request->validated();
        $post['user_id'] = $request->user()->id;
        $post['status'] = 'INREVIEW';

        $result = Post::create($post);
        if ($result) {
            return response($result, 201);
        }
        return response('Error', 400);
    }

    public function uploadImage(ImageUploadRequest $request)
    {
        $path = $request->file('image')->store('images', ['disk' => 'public_html']);

        $env = config('app.env');

        if ($env === 'production' || $env === 'staging') {
            $config = Configuration::instance([
                'cloud' => [
                  'cloud_name' => config('cloudinary.CLOUD_NAME'),
                  'api_key' => config('cloudinary.API_KEY'),
                  'api_secret' => config('cloudinary.API_SECRET')
                ],
                'url' => [
                  'secure' => true
                ]
            ]);
            
            $res = (new Cloudinary($config))->uploadApi()->upload(asset($path));
            return response(['image_url' => str_replace('http://', 'https://', $res['url'])], 200);
        }

        return response(['image_url' => $path], 200);
    }

    public function show(Post $post)
    {
        return $post;
    }

    public function destroy(Post $post)
    {
        return $post->delete();
    }

    public function updateStatus(Post $post, PostStatusRequest $statusRequest)
    {
        if ($post['status'] == 'SOLD') {
            return response(["message" => "Cannot update sold post"], 403);
        }
        $res = $post->update(['status' => request('status')]);

        if ($res) {
            return response($res, 200);
        }
        return response('Error', 400);
    }

    public function update(Post $post, UpdatePostRequest $request)
    {
        $updatedPost = $request->validated();
        $result = Post::where('id', $post['id'])->update($updatedPost);
        if ($result) {
            return response($result, 200);
        }
        return response('Error', 400);
    }
}
