<?php

namespace App\Http\Controllers;

use App\Http\Requests\ForgetPasswordRequest;
use App\Http\Requests\ResetPasswordRequest;
use App\Http\Requests\UserLoginRequest;
use App\Http\Requests\UserRegisterRequest;
use App\Models\ResetPasswordCode;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    public function register(UserRegisterRequest $request)
    {
        $request['password'] = Hash::make($request['password']);
        $request['remember_token'] = Str::random(10);
        $user = User::create($request->all());
        $token = $user->createToken('Laravel Password Grant Client')->accessToken;
        $response = array_merge($user->toArray(), ['token' => $token]);
        return response($response, 201);
    }

    public function login(UserLoginRequest $request)
    {
        $user = User::where('email', $request->email)->first();
        
        if (!Hash::check($request->password, $user->password)) {
            $response = ["message" => "Password mismatch"];
            return response($response, 422);
        }
        
        $token = $user->createToken('Laravel Password Grant Client')->accessToken;
        $response = array_merge($user->toArray(), ['token' => $token]);
        return response($response, 200);
    }

    public function logout(Request $request)
    {
        $token = $request->user()->token();
        $token->revoke();
        $response = ['message' => 'You have been successfully logged out!'];
        return response($response, 200);
    }

    public function forgotPassword(ForgetPasswordRequest $request)
    {
        ResetPasswordCode::where('email', $request->email)->delete();
        $data = ['code' => mt_rand(100000, 999999), 'email' => $request->email];
        $codeData = ResetPasswordCode::create($data);

        return response($codeData, 200);
    }

    public function resetPassword(ResetPasswordRequest $request)
    {
        $passwordReset = ResetPasswordCode::where(['code' => $request->code, 'email' => $request->email])->first();
        if ($passwordReset->created_at->addHour() < now()) {
            $passwordReset->delete();
            return response(['message' => 'Code expired.'], 400);
        }

        User::firstWhere('email', $request->email)->update(['password' => Hash::make($request->password)]);
        $passwordReset->delete();

        return response(['message' => 'Successfully changed password']);
    }
}
