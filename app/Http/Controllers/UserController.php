<?php

namespace App\Http\Controllers;

use App\Models\FcmToken;
use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    public function notifications()
    {
        return auth()->user()->notifications;
    }

    public function user(Request $request)
    {
        return $request->user();
    }

    public function posts()
    {
        return auth()->user()->posts()->paginate(10);
    }

    public function transactions()
    {
        return auth()->user()->transactions();
    }

    public function setFcmToken(Request $request)
    {
        return auth()->user()->fcmTokens()->firstOrCreate(['token' => $request->token]);
    }

    public function deleteFcmToken(Request $request)
    {
        return FcmToken::where('user_id', auth()->user()->id)->where('token', $request->token)->delete();
    }
}
