<?php

namespace App\Http\Controllers;

use App\Models\PostType;
use Illuminate\Http\Request;

class PostTypeController extends Controller
{
    public function index()
    {
        return PostType::all();
    }

    public function store(Request $request)
    {
        $postType = $request->all();
        $result = PostType::create($postType);
        if ($result) {
            return response($result, 201);
        }
        return response('Error', 400);
    }

    public function destroy(PostType $postType)
    {
        return $postType->delete();
    }
}
