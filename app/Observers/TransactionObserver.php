<?php

namespace App\Observers;

use App\Http\Services\NotificationService;
use App\Models\PointsLog;
use App\Models\Transaction;

class TransactionObserver
{
    public function creating(Transaction $transaction)
    {
        $transaction->code = $transaction->generateCode();
    }

    public function updated(Transaction $transaction)
    {
        if ($transaction->isDirty('confirmed') && $transaction->confirmed) {
            $transaction->post->update(['status' => 'SOLD']);
            NotificationService::sendPushNotification(
                [$transaction->buyer_id, $transaction->seller_id],
                'Your transaction was successful.',
                'Your transaction was successfully completed.',
                ['transaction_id' => $transaction->id, 'post_id' => $transaction->post_id]
            );
            
            PointsLog::create([
                'transaction_id' => $transaction->id,
                'user_id' => $transaction->seller_id,
                'points_earned' => $transaction->post->postType->seller_points
            ]);
            
            PointsLog::create([
                'transaction_id' => $transaction->id,
                'user_id' => $transaction->buyer_id,
                'points_earned' => $transaction->post->postType->buyer_points
            ]);
        }
    }
}
