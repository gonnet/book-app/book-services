<?php

namespace App\Observers;

use App\Models\PointsLog;
use DB;

class PointsLogObserver
{
    public function created(PointsLog $pointsLog)
    {
        $pointsLog->user()->update(['points' => DB::raw('points+'.$pointsLog->points_earned)]);
    }
    
    public function deleted(PointsLog $pointsLog){
        $pointsLog->user()->update(['points' => DB::raw('points-'.$pointsLog->points_earned)]);
    }
}
