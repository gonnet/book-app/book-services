<?php

namespace App\Models;

use App\Observers\PostObserver;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Post extends Model
{
    use HasFactory, Searchable;

    protected $fillable = ['title', 'description', 'image_url', 'number_of_papers', 'pickup_location', 'contact_method', 'user_id', 'status', 'post_type_id', 'price', 'exchange_details'];

    protected $appends = ['post_type_readable', 'user', 'created_at_readable'];

    const STATUS = ['ACTIVE', 'REJECTED', 'INREVIEW', 'SOLD'];

    protected static function boot()
    {
        parent::boot();
        self::observe(PostObserver::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function postType()
    {
        return $this->belongsTo(PostType::class);
    }

    public function getPostTypeReadableAttribute()
    {
        $post_type_readable = PostType::where('id', $this->post_type_id)->first()->name;
        return $post_type_readable;
    }

    public function getUserAttribute()
    {
        $user = User::where('id', $this->user_id)->select(['name', 'phone'])->first();
        return $user;
    }

    public function getCreatedAtReadableAttribute()
    {
        return $this->created_at->diffForHumans();
    }

    public function scopeStatus($query, $status)
    {
        return $query->where('status', $status);
    }

    public function toSearchableArray()
    {
        return [
            'title' => $this->title,
            'description' => $this->description
        ];
    }
}
