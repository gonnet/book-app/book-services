<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    const ADMIN = 1;
    const USER = 2;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'phone',
        'user_type_id',
        'points'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $attributes = [
        'user_type_id' => self::USER,
        'points' => 0
    ];

    public function posts()
    {
        return $this->hasMany(Post::class)->latest();
    }

    public function fcmTokens()
    {
        return $this->hasMany(FcmToken::class);
    }

    public function notifications()
    {
        return $this->hasMany(Notification::class)->latest();
    }

    public function type()
    {
        return $this->belongsTo(UserType::class);
    }

    public function buyerTransactions()
    {
        return $this->hasMany(Transaction::class, 'buyer_id')->latest();
    }

    public function sellerTransactions()
    {
        return $this->hasMany(Transaction::class, 'seller_id')->latest();
    }

    public function transactions()
    {
        return ['sold' => $this->sellerTransactions, 'bought' => $this->buyerTransactions];
    }

    //is_admin
    public function getIsAdminAttribute()
    {
        return $this->user_type_id === self::ADMIN;
    }

    public function scopeLeaderboard($query)
    {
        return $query->select(['id', 'name', 'points'])->orderBy('points', 'DESC');
    }
}
