<?php

namespace App\Models;

use App\Observers\TransactionObserver;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;

    protected $fillable = ['code', 'post_id', 'buyer_id', 'seller_id', 'confirmed'];

    protected $appends = ['buyer_name', 'seller_name'];

    protected static function boot()
    {
        parent::boot();
        self::observe(TransactionObserver::class);
    }

    public function generateCode()
    {
        return bin2hex(random_bytes(20));
    }

    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    public function buyer()
    {
        return $this->belongsTo(User::class, 'buyer_id');
    }

    public function seller()
    {
        return $this->belongsTo(User::class, 'seller_id');
    }

    public function getBuyerNameAttribute()
    {
        return $this->buyer->name;
    }
    
    public function getSellerNameAttribute()
    {
        return $this->seller->name;
    }
}
