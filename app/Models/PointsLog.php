<?php

namespace App\Models;

use App\Observers\PointsLogObserver;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PointsLog extends Model
{
    use HasFactory;

    protected $fillable = ['transaction_id', 'user_id', 'points_earned'];

    protected static function boot()
    {
        parent::boot();
        self::observe(PointsLogObserver::class);
    }

    public function transaction()
    {
        return $this->belongsTo(Transaction::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
