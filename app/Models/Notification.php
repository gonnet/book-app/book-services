<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'body', 'details', 'user_id', 'read_at'];

    protected $attributes = [
        'details' => '[]'
    ];

    protected $casts = [
        'details' => 'array'
    ];

    protected $appends = ['created_at_readable'];

    public function setDetailsAttribute($data)
    {
        $collection = collect($data)->toJson();
        $this->attributes['details'] = $collection;
    }

    public function getDetailsAttribute($data)
    {
        return json_decode($data);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getCreatedAtReadableAttribute()
    {
        return $this->created_at->diffForHumans();
    }
}
